describe('Testing PublisherKitClient library', () => {
  const socketIO = {
    on: (event: string, cb: () => void) => {
      cb();
      return socketIO;
    },
    connect: jest.fn(),
    connected: true,
    disconnect: jest.fn(),
  };
  beforeEach(() => {
    jest.resetModules();
    socketIO.disconnect.mockClear();
    jest.doMock('socket.io-client', () => () => socketIO);
  });
  it('should return false when library kit is initialized incorrectly', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    expect(
      publisherKitClient.initialize({
        appToken: '',
        clientApiKey: '',
        appId: '',
      }),
    ).toBe(false);
  });

  it('should return true when library kit is initialized correctly', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient._initialized = false;
    expect(
      publisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
        transports: ['websocket', 'polling'],
      }),
    ).toBe(true);
  });

  it('should return true when library is being initialized again', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    expect(
      publisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
        transports: ['websocket'],
      }),
    ).toBe(true);
    expect(
      publisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
        transports: ['websocket'],
      }),
    ).toBe(true);
  });

  it('should not allow user to connect if library is not initialized', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient.onConnect(() => {})
      .onMessage(() => {})
      .onDisconnect(() => {})
      .onError(() => {})
      .onReconnect(() => {});
    expect(publisherKitClient.connect).toThrowError();
  });

  it('should not allow user to connect if library events are not subscribed to', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    expect(publisherKitClient.connect).toThrowError();
  });

  it('should allow user to connect if library is initialized and events are subscribed to', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    publisherKitClient.onConnect(() => {}).onMessage(() => {});
    expect(publisherKitClient.connect).not.toThrowError();
  });

  it('should disconnect socket when disconnect method is called', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    publisherKitClient.onConnect(() => {}).onMessage(() => {});
    publisherKitClient.connect();
    publisherKitClient.disconnect();
    expect(socketIO.disconnect).toHaveBeenCalledTimes(1);
  });

  it('should not call disconnect socket when disconnect method is called and socket is not yet connected', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    publisherKitClient.onConnect(() => {}).onMessage(() => {});
    publisherKitClient.disconnect();
    expect(socketIO.disconnect).not.toHaveBeenCalled();
  });

  it('should call event listeners', () => {
    const onConnect = jest.fn();
    const onMessage = jest.fn();
    const onDisconnect = jest.fn();
    const onReconnect = jest.fn();
    const onError = jest.fn();
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    const publisherKitClient = new PublisherKitClient();
    publisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    publisherKitClient.onConnect(onConnect)
      .onMessage(onMessage)
      .onDisconnect(onDisconnect)
      .onError(onError)
      .onReconnect(onReconnect);
    publisherKitClient.connect();
    expect(onConnect).toHaveBeenCalledTimes(1);
    expect(onMessage).toHaveBeenCalledTimes(1);
    expect(onError).toHaveBeenCalledTimes(1);
    expect(onReconnect).toHaveBeenCalledTimes(1);
    expect(onDisconnect).toHaveBeenCalledTimes(1);
  });
});
