import io, { Socket } from 'socket.io-client';

import { EventMessage, InitOptions } from './types';

export * from './types';

export default class PublisherKitClient {
  // Token publisher will pass to authentication endpoint.
  private _appToken = '';

  // ID of the client app registered in publisher.
  private _appId = '';

  // API key of the client app registered in publisher.
  private _clientApiKey = '';

  // publisher url (protocal and port inclusive)
  private _publisherUrl = ''; // = 'http://localhost:3001';

  // The web socket used to communicate with publisher
  private _socket?: typeof Socket;

  private _initialized = false;

  private _subscribed = false;

  private _isConnected = false;

  private _userId = '';

  private _transports?: ('polling' | 'websocket')[] = ['websocket'];

  private _transportUpgrade = false;

  // Callback to be called when connection event emitted
  private _connectCallback?: () => void;

  // Callback to be called when disconnect event emitted
  private _disconnectCallback?: (reason: string) => void;

  // Callback to be called when an error occur
  private _errorCallback?: (error: Error) => void;

  // Callback to be called when reconnect event emitted
  private _reconnectCallback?: (attemptNumber: number) => void;

  // Callback to be called when message event emitted
  private _messageCallback?: (message: EventMessage) => void;

  // Get the url used to connect web socket to
  private _socketUrl() {
    return `${this._publisherUrl}/${this._appId}`;
  }

  initialize = (options: InitOptions) => {
    if (!this._initialized) {
      const {
        appId,
        appToken,
        clientApiKey,
        publisherUrl,
        userId,
        transports,
        transportUpgrade,
      } = options;
      if (!appId || !clientApiKey) {
        return false;
      }
      this._appId = appId;
      this._clientApiKey = clientApiKey;
      if (userId) {
        this._userId = userId;
      }
      if (appToken) {
        this._appToken = appToken;
      }
      if (transports) {
        this._transports = transports;
      }
      this._transportUpgrade = !!transportUpgrade;
      this._publisherUrl = publisherUrl || this._publisherUrl;
      this._initialized = true;
    }
    return this._initialized;
  };

  updateAccessParams = (params: { token: string; userId: string }) => {
    const { token, userId } = params;
    this._appToken = token;
    this._userId = userId;
  };

  connect = (opts?: { upgrade: boolean }) => {
    if (this._initialized && this._subscribed) {
      if (this._isConnected) {
        this._socket?.disconnect();
      }
      this._socket = io(this._socketUrl(), {
        transports: this._transports,
        autoConnect: false,
        forceNew: false,
        upgrade: opts?.upgrade || this._transportUpgrade,
        query: {
          token: this._appToken,
          appId: this._appId,
          clientApiKey: this._clientApiKey,
          userId: this._userId,
        },
      });
      this._socket
        .on('connect', () => {
          this._connectCallback?.();
          this._isConnected = true;
        })
        .on('disconnect', (reason: string) => {
          this._disconnectCallback?.(reason);
          this._isConnected = false;
        })
        .on('error', (error: Error) => {
          this._errorCallback?.(error);
        })
        .on('reconnect', (attemptNumber: number) => {
          this._reconnectCallback?.(attemptNumber);
        })
        .on('message', (message: EventMessage) => {
          this._messageCallback?.(message);
        });
      this._socket.connect();
    } else {
      if (!this._initialized) {
        throw new Error('Please initialize before connecting');
      }

      if (!this._subscribed) {
        throw new Error('Please register/subscribe to "onMessage" callback before connecting');
      }

      throw new Error('Please check your initialization parameters');
    }
  };

  isConnected = () => this._isConnected;

  disconnect = () => {
    if (this._socket && this._socket.connected) {
      this._socket.disconnect();
    }
  };

  onConnect = (cb: () => void) => {
    this._connectCallback = cb;
    return this;
  };

  onDisconnect = (cb: (reason: string) => void) => {
    this._disconnectCallback = cb;
    return this;
  };

  onError = (cb: (e: Error) => void) => {
    this._errorCallback = cb;
    return this;
  };

  onReconnect = (cb: (attemptNumber: number) => void) => {
    this._reconnectCallback = cb;
    return this;
  };

  onMessage = (cb: (message: EventMessage) => void) => {
    this._messageCallback = cb;
    this._subscribed = true;
    return this;
  };
}
