(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('socket.io-client')) :
  typeof define === 'function' && define.amd ? define(['socket.io-client'], factory) :
  (global = global || self, global.PublisherKitClient = factory(global.io));
}(this, (function (io) { 'use strict';

  io = io && io.hasOwnProperty('default') ? io['default'] : io;

  var PublisherKitClient = /** @class */ (function () {
      function PublisherKitClient() {
          var _this = this;
          // Token publisher will pass to authentication endpoint.
          this._appToken = '';
          // ID of the client app registered in publisher.
          this._appId = '';
          // API key of the client app registered in publisher.
          this._clientApiKey = '';
          // publisher url (protocal and port inclusive)
          this._publisherUrl = ''; // = 'http://localhost:3001';
          this._initialized = false;
          this._subscribed = false;
          this._isConnected = false;
          this._userId = '';
          this._transports = ['websocket'];
          this._transportUpgrade = false;
          this.initialize = function (options) {
              if (!_this._initialized) {
                  var appId = options.appId, appToken = options.appToken, clientApiKey = options.clientApiKey, publisherUrl = options.publisherUrl, userId = options.userId, transports = options.transports, transportUpgrade = options.transportUpgrade;
                  if (!appId || !clientApiKey) {
                      return false;
                  }
                  _this._appId = appId;
                  _this._clientApiKey = clientApiKey;
                  if (userId) {
                      _this._userId = userId;
                  }
                  if (appToken) {
                      _this._appToken = appToken;
                  }
                  if (transports) {
                      _this._transports = transports;
                  }
                  _this._transportUpgrade = !!transportUpgrade;
                  _this._publisherUrl = publisherUrl || _this._publisherUrl;
                  _this._initialized = true;
              }
              return _this._initialized;
          };
          this.updateAccessParams = function (params) {
              var token = params.token, userId = params.userId;
              _this._appToken = token;
              _this._userId = userId;
          };
          this.connect = function (opts) {
              var _a, _b;
              if (_this._initialized && _this._subscribed) {
                  if (_this._isConnected) {
                      (_a = _this._socket) === null || _a === void 0 ? void 0 : _a.disconnect();
                  }
                  _this._socket = io(_this._socketUrl(), {
                      transports: _this._transports,
                      autoConnect: false,
                      forceNew: false,
                      upgrade: ((_b = opts) === null || _b === void 0 ? void 0 : _b.upgrade) || _this._transportUpgrade,
                      query: {
                          token: _this._appToken,
                          appId: _this._appId,
                          clientApiKey: _this._clientApiKey,
                          userId: _this._userId,
                      },
                  });
                  _this._socket
                      .on('connect', function () {
                      var _a, _b;
                      (_b = (_a = _this)._connectCallback) === null || _b === void 0 ? void 0 : _b.call(_a);
                      _this._isConnected = true;
                  })
                      .on('disconnect', function (reason) {
                      var _a, _b;
                      (_b = (_a = _this)._disconnectCallback) === null || _b === void 0 ? void 0 : _b.call(_a, reason);
                      _this._isConnected = false;
                  })
                      .on('error', function (error) {
                      var _a, _b;
                      (_b = (_a = _this)._errorCallback) === null || _b === void 0 ? void 0 : _b.call(_a, error);
                  })
                      .on('reconnect', function (attemptNumber) {
                      var _a, _b;
                      (_b = (_a = _this)._reconnectCallback) === null || _b === void 0 ? void 0 : _b.call(_a, attemptNumber);
                  })
                      .on('message', function (message) {
                      var _a, _b;
                      (_b = (_a = _this)._messageCallback) === null || _b === void 0 ? void 0 : _b.call(_a, message);
                  });
                  _this._socket.connect();
              }
              else {
                  if (!_this._initialized) {
                      throw new Error('Please initialize before connecting');
                  }
                  if (!_this._subscribed) {
                      throw new Error('Please register/subscribe to "onMessage" callback before connecting');
                  }
                  throw new Error('Please check your initialization parameters');
              }
          };
          this.isConnected = function () { return _this._isConnected; };
          this.disconnect = function () {
              if (_this._socket && _this._socket.connected) {
                  _this._socket.disconnect();
              }
          };
          this.onConnect = function (cb) {
              _this._connectCallback = cb;
              return _this;
          };
          this.onDisconnect = function (cb) {
              _this._disconnectCallback = cb;
              return _this;
          };
          this.onError = function (cb) {
              _this._errorCallback = cb;
              return _this;
          };
          this.onReconnect = function (cb) {
              _this._reconnectCallback = cb;
              return _this;
          };
          this.onMessage = function (cb) {
              _this._messageCallback = cb;
              _this._subscribed = true;
              return _this;
          };
      }
      // Get the url used to connect web socket to
      PublisherKitClient.prototype._socketUrl = function () {
          return this._publisherUrl + "/" + this._appId;
      };
      return PublisherKitClient;
  }());

  return PublisherKitClient;

})));
//# sourceMappingURL=publisher-kit-client.umd.js.map
