export interface InitOptions {
    appToken?: string;
    appId: string;
    clientApiKey: string;
    publisherUrl?: string;
    userId?: string;
    transports?: ('polling' | 'websocket')[];
    transportUpgrade?: boolean;
}
export interface EventMessage {
    event: string;
    payload: any;
}
