import { EventMessage, InitOptions } from './types';
export * from './types';
export default class PublisherKitClient {
    private _appToken;
    private _appId;
    private _clientApiKey;
    private _publisherUrl;
    private _socket?;
    private _initialized;
    private _subscribed;
    private _isConnected;
    private _userId;
    private _transports?;
    private _transportUpgrade;
    private _connectCallback?;
    private _disconnectCallback?;
    private _errorCallback?;
    private _reconnectCallback?;
    private _messageCallback?;
    private _socketUrl;
    initialize: (options: InitOptions) => boolean;
    updateAccessParams: (params: {
        token: string;
        userId: string;
    }) => void;
    connect: (opts?: {
        upgrade: boolean;
    } | undefined) => void;
    isConnected: () => boolean;
    disconnect: () => void;
    onConnect: (cb: () => void) => this;
    onDisconnect: (cb: (reason: string) => void) => this;
    onError: (cb: (e: Error) => void) => this;
    onReconnect: (cb: (attemptNumber: number) => void) => this;
    onMessage: (cb: (message: EventMessage) => void) => this;
}
