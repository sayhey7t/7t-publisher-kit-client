[@7t/publisher-kit-client - v2.0.0](../README.md) › [Globals](../globals.md) › [PublisherKitClient](publisherkitclient.md)

# Class: PublisherKitClient

## Hierarchy

- **PublisherKitClient**

## Index

### Properties

- [\_appId](publisherkitclient.md#markdown-header-private-_appid)
- [\_appToken](publisherkitclient.md#markdown-header-private-_apptoken)
- [\_clientApiKey](publisherkitclient.md#markdown-header-private-_clientapikey)
- [\_connectCallback](publisherkitclient.md#markdown-header-private-optional-_connectcallback)
- [\_disconnectCallback](publisherkitclient.md#markdown-header-private-optional-_disconnectcallback)
- [\_errorCallback](publisherkitclient.md#markdown-header-private-optional-_errorcallback)
- [\_initialized](publisherkitclient.md#markdown-header-private-_initialized)
- [\_isConnected](publisherkitclient.md#markdown-header-private-_isconnected)
- [\_messageCallback](publisherkitclient.md#markdown-header-private-optional-_messagecallback)
- [\_publisherUrl](publisherkitclient.md#markdown-header-private-_publisherurl)
- [\_reconnectCallback](publisherkitclient.md#markdown-header-private-optional-_reconnectcallback)
- [\_socket](publisherkitclient.md#markdown-header-private-optional-_socket)
- [\_subscribed](publisherkitclient.md#markdown-header-private-_subscribed)
- [\_transportUpgrade](publisherkitclient.md#markdown-header-private-_transportupgrade)
- [\_transports](publisherkitclient.md#markdown-header-private-optional-_transports)
- [\_userId](publisherkitclient.md#markdown-header-private-_userid)

### Methods

- [\_socketUrl](publisherkitclient.md#markdown-header-private-_socketurl)
- [connect](publisherkitclient.md#markdown-header-connect)
- [disconnect](publisherkitclient.md#markdown-header-disconnect)
- [initialize](publisherkitclient.md#markdown-header-initialize)
- [isConnected](publisherkitclient.md#markdown-header-isconnected)
- [onConnect](publisherkitclient.md#markdown-header-onconnect)
- [onDisconnect](publisherkitclient.md#markdown-header-ondisconnect)
- [onError](publisherkitclient.md#markdown-header-onerror)
- [onMessage](publisherkitclient.md#markdown-header-onmessage)
- [onReconnect](publisherkitclient.md#markdown-header-onreconnect)
- [updateAccessParams](publisherkitclient.md#markdown-header-updateaccessparams)

## Properties

### `Private` \_appId

• **\_appId**: _string_ = ""

Defined in publisher-kit-client.ts:12

---

### `Private` \_appToken

• **\_appToken**: _string_ = ""

Defined in publisher-kit-client.ts:9

---

### `Private` \_clientApiKey

• **\_clientApiKey**: _string_ = ""

Defined in publisher-kit-client.ts:15

---

### `Private` `Optional` \_connectCallback

• **\_connectCallback**? : _undefined | function_

Defined in publisher-kit-client.ts:36

---

### `Private` `Optional` \_disconnectCallback

• **\_disconnectCallback**? : _undefined | function_

Defined in publisher-kit-client.ts:39

---

### `Private` `Optional` \_errorCallback

• **\_errorCallback**? : _undefined | function_

Defined in publisher-kit-client.ts:42

---

### `Private` \_initialized

• **\_initialized**: _boolean_ = false

Defined in publisher-kit-client.ts:23

---

### `Private` \_isConnected

• **\_isConnected**: _boolean_ = false

Defined in publisher-kit-client.ts:27

---

### `Private` `Optional` \_messageCallback

• **\_messageCallback**? : _undefined | function_

Defined in publisher-kit-client.ts:48

---

### `Private` \_publisherUrl

• **\_publisherUrl**: _string_ = ""

Defined in publisher-kit-client.ts:18

---

### `Private` `Optional` \_reconnectCallback

• **\_reconnectCallback**? : _undefined | function_

Defined in publisher-kit-client.ts:45

---

### `Private` `Optional` \_socket

• **\_socket**? : _undefined | Socket_

Defined in publisher-kit-client.ts:21

---

### `Private` \_subscribed

• **\_subscribed**: _boolean_ = false

Defined in publisher-kit-client.ts:25

---

### `Private` \_transportUpgrade

• **\_transportUpgrade**: _boolean_ = false

Defined in publisher-kit-client.ts:33

---

### `Private` `Optional` \_transports

• **\_transports**? : _"polling" | "websocket"[]_ = ['websocket']

Defined in publisher-kit-client.ts:31

---

### `Private` \_userId

• **\_userId**: _string_ = ""

Defined in publisher-kit-client.ts:29

## Methods

### `Private` \_socketUrl

▸ **\_socketUrl**(): _string_

Defined in publisher-kit-client.ts:51

**Returns:** _string_

---

### connect

▸ **connect**(`opts?`: undefined | object): _void_

Defined in publisher-kit-client.ts:93

**Parameters:**

| Name    | Type                    |
| ------- | ----------------------- |
| `opts?` | undefined &#124; object |

**Returns:** _void_

---

### disconnect

▸ **disconnect**(): _void_

Defined in publisher-kit-client.ts:144

**Returns:** _void_

---

### initialize

▸ **initialize**(`options`: [InitOptions](../interfaces/initoptions.md)): _boolean_

Defined in publisher-kit-client.ts:55

**Parameters:**

| Name      | Type                                        |
| --------- | ------------------------------------------- |
| `options` | [InitOptions](../interfaces/initoptions.md) |

**Returns:** _boolean_

---

### isConnected

▸ **isConnected**(): _boolean_

Defined in publisher-kit-client.ts:142

**Returns:** _boolean_

---

### onConnect

▸ **onConnect**(`cb`: function): _this_

Defined in publisher-kit-client.ts:150

**Parameters:**

▪ **cb**: _function_

▸ (): _void_

**Returns:** _this_

---

### onDisconnect

▸ **onDisconnect**(`cb`: function): _this_

Defined in publisher-kit-client.ts:155

**Parameters:**

▪ **cb**: _function_

▸ (`reason`: string): _void_

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `reason` | string |

**Returns:** _this_

---

### onError

▸ **onError**(`cb`: function): _this_

Defined in publisher-kit-client.ts:160

**Parameters:**

▪ **cb**: _function_

▸ (`e`: Error): _void_

**Parameters:**

| Name | Type  |
| ---- | ----- |
| `e`  | Error |

**Returns:** _this_

---

### onMessage

▸ **onMessage**(`cb`: function): _this_

Defined in publisher-kit-client.ts:170

**Parameters:**

▪ **cb**: _function_

▸ (`message`: [EventMessage](../interfaces/eventmessage.md)): _void_

**Parameters:**

| Name      | Type                                          |
| --------- | --------------------------------------------- |
| `message` | [EventMessage](../interfaces/eventmessage.md) |

**Returns:** _this_

---

### onReconnect

▸ **onReconnect**(`cb`: function): _this_

Defined in publisher-kit-client.ts:165

**Parameters:**

▪ **cb**: _function_

▸ (`attemptNumber`: number): _void_

**Parameters:**

| Name            | Type   |
| --------------- | ------ |
| `attemptNumber` | number |

**Returns:** _this_

---

### updateAccessParams

▸ **updateAccessParams**(`params`: object): _void_

Defined in publisher-kit-client.ts:87

**Parameters:**

▪ **params**: _object_

| Name     | Type   |
| -------- | ------ |
| `token`  | string |
| `userId` | string |

**Returns:** _void_
