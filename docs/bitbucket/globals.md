[@7t/publisher-kit-client - v2.0.0](README.md) › [Globals](globals.md)

# @7t/publisher-kit-client - v2.0.0

## Index

### Classes

- [PublisherKitClient](classes/publisherkitclient.md)

### Interfaces

- [EventMessage](interfaces/eventmessage.md)
- [InitOptions](interfaces/initoptions.md)
