[@7t/publisher-kit-client - v2.0.0](../README.md) › [Globals](../globals.md) › [EventMessage](eventmessage.md)

# Interface: EventMessage

## Hierarchy

- **EventMessage**

## Index

### Properties

- [event](eventmessage.md#markdown-header-event)
- [payload](eventmessage.md#markdown-header-payload)

## Properties

### event

• **event**: _string_

Defined in types.ts:12

---

### payload

• **payload**: _any_

Defined in types.ts:13
