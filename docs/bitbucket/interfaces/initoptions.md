[@7t/publisher-kit-client - v2.0.0](../README.md) › [Globals](../globals.md) › [InitOptions](initoptions.md)

# Interface: InitOptions

## Hierarchy

- **InitOptions**

## Index

### Properties

- [appId](initoptions.md#markdown-header-appid)
- [appToken](initoptions.md#markdown-header-optional-apptoken)
- [clientApiKey](initoptions.md#markdown-header-clientapikey)
- [publisherUrl](initoptions.md#markdown-header-optional-publisherurl)
- [transportUpgrade](initoptions.md#markdown-header-optional-transportupgrade)
- [transports](initoptions.md#markdown-header-optional-transports)
- [userId](initoptions.md#markdown-header-optional-userid)

## Properties

### appId

• **appId**: _string_

Defined in types.ts:3

---

### `Optional` appToken

• **appToken**? : _undefined | string_

Defined in types.ts:2

---

### clientApiKey

• **clientApiKey**: _string_

Defined in types.ts:4

---

### `Optional` publisherUrl

• **publisherUrl**? : _undefined | string_

Defined in types.ts:5

---

### `Optional` transportUpgrade

• **transportUpgrade**? : _undefined | false | true_

Defined in types.ts:8

---

### `Optional` transports

• **transports**? : _"polling" | "websocket"[]_

Defined in types.ts:7

---

### `Optional` userId

• **userId**? : _undefined | string_

Defined in types.ts:6
